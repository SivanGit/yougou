// 0 引入 用来发送请求的 方法
import { request } from "../../request/index.js"
Page({
    data: {
        // 轮播图数组
        swiperList: [],
        catesList: [],
        floorList: []
    },
    // 页面开始加载 就会被触发
    onLoad: function(options) {
        // 1 发送异步请求获取轮播图数据
        // wx.request({
        //     url: 'https://api-hmugo-web.itheima.net/api/public/v1/home/swiperdata',
        //     success: (result) => {
        //         this.setData({
        //             swiperList: result.data.message
        //         })
        //     }
        // });
        this.getSwiperList();
        this.getCateList();
        this.getFloorList();
    },
    // 获取轮播图数据
    getSwiperList() {
        request({ url: '/home/swiperdata' })
            .then(result => {
                result.forEach((v, i) => {
                    result[i].navigator_url = v.navigator_url.replace('main','goods_detail');
                    // console.log(v.navigator_url);
                });
                this.setData({
                    swiperList: result
                });
            })
    },
    // 获取 分类导航数据
    getCateList() {
        request({ url: '/home/catitems' })
            .then(result => {
                this.setData({
                    catesList: result
                })
            })
    },
    // 获取 楼层数据
    getFloorList() {
        request({ url: '/home/floordata' })
            .then(result => {
                result.forEach(v1 => {
                    v1.product_list.forEach(v2 => {
                      v2.navigator_url = v2.navigator_url.replace('goods_list', 'goods_list/goods_list')
                    //   console.log(v2.navigator_url);
                    })
                  });
                this.setData({
                    floorList: result
                })
            })
    }
});