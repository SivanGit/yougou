// pages/user/user.js
Page({
  data:{
    userinfo:{},
    // 被收藏的商品的数量
    collectNums:0
  },
  
  onShow: function () {
    // this.data.userinfo = wx.getStorageSync("userInfo");
    const userinfo = wx.getStorageSync("userInfo");
    const collect = wx.getStorageSync("collect")||[];
    this.setData({
      userinfo,
      collectNums:collect.length
    })
  }
})