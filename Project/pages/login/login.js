// pages/login/login.js
Page({
  getUserProfile(){
    // wx.login({
    //   timeout:10000,
    //   success: (result)=>{
    //     const {code} = result;
    //   }
    // });
    wx.getUserProfile({
      desc: '用于完善会员资料', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
        console.log(res);
        const {
          userInfo
        } = res;
        console.log(userInfo);
        wx.setStorageSync('userInfo', userInfo);
        wx.navigateBack({
          delta: 1
        });
      }
    })
  }
})
// 7810fd410f9fdba6c2714a5791e644a2